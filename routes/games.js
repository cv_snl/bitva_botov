const express = require('express')
const router  = express.Router()

const RESULT = { status: 'ok' }
const RESULT_MOVE = { status: 'ok', move_from: [0,2], move_to: [0,3] }

router.post('/', (req, res) => {
  gamesList[req.body.id] = {
    first_turn: req.body.first_turn,
    training: req.body.training,
    jumps: req.body.jumps,
    board: req.body.board
  }
  
  res.json(RESULT)
})

router.get('/:id', (req, res) => {
  let { board } = gamesList[req.params.id]

  console.log(board)
  res.json(RESULT_MOVE)
})

router.put('/:id', (req, res) => {
  res.json(RESULT)
})

router.delete('/:id', (req, res) => {
  delete gamesList[req.params.id]
  res.json(RESULT)
})

module.exports = router
